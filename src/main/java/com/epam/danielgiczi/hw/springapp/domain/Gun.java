package com.epam.danielgiczi.hw.springapp.domain;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;

public class Gun {
	final String name;
	final int damage;
	final double chanceToHit;
	final ShotFactory shotFactory;

	public Gun(String name, int damage, double chanceToHit, ShotFactory shotFactory) {
		super();
		this.name = name;
		this.damage = damage;
		this.chanceToHit = chanceToHit;
		this.shotFactory = shotFactory;
	}

	@Override
	public String toString() {
		return "Gun [name=" + name + "]";
	}

	public String getName() {
		return name;
	}

	public Shot nextShot() {
		Shot shot = shotFactory.createShot(damage, chanceToHit);
		return shot;		
	}
	
	public double getMaximumDamage(){
		double maximumDamage = shotFactory.calculateMaximumDamage(damage);
		return maximumDamage;
	}

}

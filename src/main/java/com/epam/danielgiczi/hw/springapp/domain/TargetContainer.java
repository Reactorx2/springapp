package com.epam.danielgiczi.hw.springapp.domain;

public class TargetContainer {
	
	final Target entity;
	private int health;
	
	public TargetContainer(Target entitiy) {
		this.entity = entitiy;
		health = entity.getStartingHealth();
	}

	public void hit(int damage){
		if(isEntityAlive()){
			health = health-damage;
		}
	}
	
	public boolean isEntityAlive(){
		return getHealth() > 0;
	}
	
	public Target asTarget(){
		return entity;
	}

	public int getHealth() {
		return health;
	}

}

package com.epam.danielgiczi.hw.springapp.domain;

public class Hound {
	private final String name;
	private final HuntsMan huntsMan;
	
	public Hound(String name, HuntsMan huntsMan) {
		super();
		this.name = name;
		this.huntsMan = huntsMan;
	}

	@Override
	public String toString() {
		return "Hound [name=" + name + ", huntsMan=" + huntsMan + "]";
	}

	public String getName() {
		return name;
	}

	public HuntsMan getHuntsMan() {
		return huntsMan;
	}

}

package com.epam.danielgiczi.hw.springapp.domain.event;

import java.text.MessageFormat;

import com.epam.danielgiczi.hw.springapp.domain.ForestEntity;

public class MissedShotEvent extends ShotEvent {
	
	public MissedShotEvent(ForestEntity shooter, ForestEntity target) {
		super(shooter, target, 0);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getMessage() {
		return MessageFormat.format("{0} missed the shot.", who.getName(), whom.getName());
	}

}

package com.epam.danielgiczi.hw.springapp.domain.event;

import java.util.List;

import com.epam.danielgiczi.hw.springapp.domain.Shooter;
import com.epam.danielgiczi.hw.springapp.domain.Shot;
import com.epam.danielgiczi.hw.springapp.domain.Target;
import com.epam.danielgiczi.hw.springapp.domain.TargetContainer;

public class EventFacade {
	public EventFacade() {}
	
	public ShotEvent createShotEvent(Shooter shooter, Target target, Shot shot){
		ShotEvent event = null;
		if(shot.isHit()){
			if(shot.isCritical()){
				event = new CriticalHitEvent(shooter, target, shot.getDamage());
			} else {
				event = new HitEvent(shooter, target, shot.getDamage());
			}
		} else {
			event = new MissedShotEvent(shooter, target);
		}
		
		return event;
	}
	
	public EntityHurtEvent createEntityHurtEvent(TargetContainer container, Shot shot){
		EntityHurtEvent event = null;
		Target target = container.asTarget();
		if(container.isEntityAlive()){
			event = new EntityHurtEvent(target, container.getHealth());
		} else {
			event = new EntityDownEvent(target, container.getHealth());
		}
		return event;
	}

	public HuntingAccidentEvent createHuntingAccidentEvent(TargetContainer targetContainer) {
		return new HuntingAccidentEvent(targetContainer.asTarget());
	}
	
	public ShootersAreDownEvent createShootersAreDownEvent(List<Target> targetsAlive){
		return new ShootersAreDownEvent(targetsAlive);
	}
}

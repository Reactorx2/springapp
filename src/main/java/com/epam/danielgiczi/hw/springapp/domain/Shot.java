package com.epam.danielgiczi.hw.springapp.domain;

public class Shot {
	private final int damage;
	private final boolean critical;
	private final boolean hit;
	
	public Shot(int damage, boolean hit, boolean critical) {
		super();
		this.damage = damage;
		this.hit = hit;
		this.critical = critical;
	}

	public int getDamage() {
		return damage;
	}

	public boolean isHit() {
		return hit;
	}

	public boolean isCritical() {
		return critical;
	}

}

package com.epam.danielgiczi.hw.springapp.simulator;

import java.util.Observable;
import java.util.Observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.danielgiczi.hw.springapp.domain.event.SimulationEvent;

public class HuntingEventLogger implements Observer {
	
	final protected Logger logger = LoggerFactory.getLogger(HuntingSimulator.class);

	public void update(Observable o, Object arg) {
		if(o instanceof HuntingSimulator && arg instanceof SimulationEvent){
			HuntingSimulator simulator = (HuntingSimulator)o;
			SimulationEvent event = (SimulationEvent)arg;
			handleEvent(simulator, event);
		} else {
			logger.warn("Unexpected update method invoke from " + o);
		}
	}
	
	public void handleEvent(HuntingSimulator simulator, SimulationEvent event){
		logger.info(event.getMessage());
	}
	
}

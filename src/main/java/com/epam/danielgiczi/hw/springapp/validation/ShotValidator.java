package com.epam.danielgiczi.hw.springapp.validation;

import com.epam.danielgiczi.hw.springapp.domain.Gun;
import com.epam.danielgiczi.hw.springapp.domain.Shot;

public class ShotValidator {
    
    public boolean isValid(Shot shot, Gun gun){
        return  (gun.getMaximumDamage() >= shot.getDamage());
    }
    
}

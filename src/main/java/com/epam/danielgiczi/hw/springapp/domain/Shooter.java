package com.epam.danielgiczi.hw.springapp.domain;

public interface Shooter extends Target {
	public Shot nextShot();
	public double getMaximumDamage();
}

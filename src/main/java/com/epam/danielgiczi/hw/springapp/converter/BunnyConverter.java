package com.epam.danielgiczi.hw.springapp.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.convert.converter.Converter;

import com.epam.danielgiczi.hw.springapp.domain.Bunny;

public class BunnyConverter implements Converter<String, Bunny> {

	@Autowired
	private ApplicationContext appContext;
	
	public Bunny convert(String source) {
		String[] parts = source.split(";");

		int partCounter = 0;
		final String name = parts[partCounter++];
		final int ageInDays = Integer.parseInt(parts[partCounter++]);
		final Bunny.Color color = Bunny.Color.valueOf(parts[partCounter++]);
		final Bunny father = (Bunny)appContext.getBean(parts[partCounter++]);
		final Bunny mother = (Bunny)appContext.getBean(parts[partCounter++]);
		
        return new Bunny(name, ageInDays, color, father, mother);
	}
	
}

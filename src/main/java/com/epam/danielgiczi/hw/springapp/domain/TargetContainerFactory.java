package com.epam.danielgiczi.hw.springapp.domain;

public class TargetContainerFactory {
	public TargetContainer createContainerFor(Target target){
		return new TargetContainer(target);
	}
}

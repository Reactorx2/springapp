package com.epam.danielgiczi.hw.springapp.domain;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component("shotFactory")
public class ShotFactory {
    private final double maxDamageDifferenceRatio;
    private final double criticalHitChance;
    private final double maxCriticalDamageRatio;
    private final double minCriticalDamageRatio;

    final Random random = new Random();

    public ShotFactory(double maxDamageDifferenceRatio, double criticalHitChance, double maxCriticalDamageRatio, double minCriticalDamageRatio) {
        super();
        this.maxDamageDifferenceRatio = maxDamageDifferenceRatio;
        this.criticalHitChance = criticalHitChance;
        this.maxCriticalDamageRatio = maxCriticalDamageRatio;
        this.minCriticalDamageRatio = minCriticalDamageRatio;
    }

    public Shot createShot(int damage, double chanceToHit) {
		Shot shot;
		if(chanceToHit <= random.nextDouble()){
		    final boolean isCriticalHit = random.nextDouble() <= criticalHitChance;
		    double criticalDamage = 0;
	        if(isCriticalHit){
	            criticalDamage = calculateCriticalDamage(damage);
	        }
			final double shotDamageOffset = randomDamageOffset(damage);
			final double shotDamage = damage + shotDamageOffset + criticalDamage;
			
			shot = new Shot((int)shotDamage, true, isCriticalHit);
		} else {
			shot = new Shot(0, false, false);
		}
		return shot;
	}

	private double randomDamageOffset(int damage) {
		return (-1 + random.nextDouble() * 2) * calculateMaximumDamageDifference(damage);
	}

	private double calculateCriticalDamage(int damage) {
			return damage * random.nextDouble() * (maxCriticalDamageRatio - minCriticalDamageRatio);
	}

    private double calculateMaximumCriticalDamageExtra(int damage) {
        return maxCriticalDamageRatio * damage;
    }

    public double calculateMaximumDamageDifference(int damage) {
        return damage * maxDamageDifferenceRatio;
    }
    
    public double calculateMaximumDamage(int damage) {
        double maximumDamage = damage + calculateMaximumDamageDifference(damage) + calculateMaximumCriticalDamageExtra(damage);
        return maximumDamage;
    }

}

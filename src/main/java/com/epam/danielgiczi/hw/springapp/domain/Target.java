package com.epam.danielgiczi.hw.springapp.domain;

public interface Target extends ForestEntity {
	public int getStartingHealth();
}

package com.epam.danielgiczi.hw.springapp.aspect;

import javax.naming.OperationNotSupportedException;

import org.aspectj.lang.annotation.Pointcut;

public class HuntsManPointcuts {
    private HuntsManPointcuts() throws OperationNotSupportedException {
        throw new OperationNotSupportedException();
    }
    
    @Pointcut("execution(public com.epam.danielgiczi.hw.springapp.domain.Shot com.epam.danielgiczi.hw.springapp.domain.AbstractShooter.nextShot())")
    public static void shooterNextShot() {}
}

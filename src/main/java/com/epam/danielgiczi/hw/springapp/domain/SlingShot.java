package com.epam.danielgiczi.hw.springapp.domain;

public class SlingShot extends Gun {

    public SlingShot(String name, int damage, double chanceToHit, ShotFactory shotFactory) {
        super(name, damage, chanceToHit, shotFactory);
    }

    @Override
    public String toString() {
        return "SlingShot [name=" + name + ", damage=" + damage + ", chanceToHit=" + chanceToHit + ", shotFactory=" + shotFactory + "]";
    }

}

package com.epam.danielgiczi.hw.springapp.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.epam.danielgiczi.hw.springapp.domain.AbstractShooter;

@Aspect
@Component
@Order(value=80)
public class GunExistenceChecker {
    
    static final Logger logger = LoggerFactory.getLogger(GunExistenceChecker.class);
    
    @Before("HuntsManPointcuts.shooterNextShot() && target(shooter)")
    public void provideSlingShot(JoinPoint joinPoint, AbstractShooter shooter){
        if(shooter.getGun() == null){
            throw new IllegalStateException("Can't shoot without weapon");
        }
    }
    
}

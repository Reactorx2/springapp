package com.epam.danielgiczi.hw.springapp.domain;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

public class Forest {
	private final String name;
	private final LinkedList<Bunny> bunnies;
	private final LinkedList<HuntsMan> hunters;
	
	protected Forest(String name, LinkedList<Bunny> bunnies, LinkedList<HuntsMan> hunters) {
		super();
		this.name = name;
		this.bunnies = bunnies;
		this.hunters = hunters;
	}

	public String getName() {
		return name;
	}

	public Collection<Bunny> getBunnies() {
		return Collections.unmodifiableCollection(bunnies);
	}

	public Collection<HuntsMan> getHunters() {
		return Collections.unmodifiableCollection(hunters);
	}
	
	public Collection<Target> getTargets(){
		LinkedList<Target> targets = new LinkedList<Target>();
		targets.addAll(hunters);
		targets.addAll(bunnies);
		return targets;
	}
	
	
}

package com.epam.danielgiczi.hw.springapp;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.epam.danielgiczi.hw.springapp.simulator.HuntingEventLogger;
import com.epam.danielgiczi.hw.springapp.simulator.HuntingSimulator;

public class App {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(JavaConfig.class);
		
		HuntingSimulator huntingSimulator = (HuntingSimulator) context.getBean("theSimulator");
		HuntingEventLogger eventLogger = new HuntingEventLogger();
		huntingSimulator.addObserver(eventLogger);
		huntingSimulator.simulate();

		context.close();
	}
}

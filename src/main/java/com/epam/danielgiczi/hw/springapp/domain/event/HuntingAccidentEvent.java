package com.epam.danielgiczi.hw.springapp.domain.event;

import java.text.MessageFormat;

import com.epam.danielgiczi.hw.springapp.domain.ForestEntity;

public class HuntingAccidentEvent extends SimulationEvent {
	final ForestEntity victim;
	
	public HuntingAccidentEvent(ForestEntity victim) {
		super();
		this.victim = victim;
	}

	@Override
	public String getMessage() {
		return MessageFormat.format("Hunting accident occurred! Poor guy: {0}", victim.getName());
	}

}

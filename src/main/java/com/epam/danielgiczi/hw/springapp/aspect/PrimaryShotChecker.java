package com.epam.danielgiczi.hw.springapp.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.epam.danielgiczi.hw.springapp.domain.AbstractShooter;
import com.epam.danielgiczi.hw.springapp.domain.Gun;
import com.epam.danielgiczi.hw.springapp.domain.Shot;
import com.epam.danielgiczi.hw.springapp.validation.ShotValidator;

@Aspect
@Component("primaryShotChecker")
@Order(value=100)
public class PrimaryShotChecker {
    
    private static final Logger logger = LoggerFactory.getLogger(PrimaryShotChecker.class);
    
    @Autowired(required=true)
    private ShotValidator shotValidator; 
    
    @Around("HuntsManPointcuts.shooterNextShot()")
    public Object checkShot(ProceedingJoinPoint joinPoint) throws Throwable {
        Shot shot = (Shot) joinPoint.proceed();
        AbstractShooter abstractShooter = (AbstractShooter)joinPoint.getTarget();
        Gun gun = abstractShooter.getGun();
        
        if(!shotValidator.isValid(shot, gun)){
            logger.info("Invalid shot detected! Trying to fix by generating a new one.");
            shot = abstractShooter.getGun().nextShot(); 
        }
        
        return shot;
    }
    
}

package com.epam.danielgiczi.hw.springapp.domain;

import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrickyHuntsMan extends HuntsMan {

	private static final double CHEAT_LIKELIHOOD = 0.5;

	public TrickyHuntsMan(String name, String address, Gun gun, int health) {
		super(name, address, gun, health);
	}
	
	@Override
	public Shot nextShot(){
		Shot shot = super.nextShot();
		double randValue = Math.random();
		if(randValue > CHEAT_LIKELIHOOD){
			tryHackShot(shot);
		} else {
			// Leave it in original state
		}
		return shot;
	}

	private void tryHackShot(Shot shot) {
		try {
			Field damageField = Shot.class.getDeclaredField("damage");
			damageField.setAccessible(true);
			damageField.setInt(shot, 10000);
		} catch (Exception e){
			Logger logger = LoggerFactory.getLogger(TrickyHuntsMan.class);
			logger.error("Could not hack the shot object!");
			logger.error(e.toString());
		}
	}

}

package com.epam.danielgiczi.hw.springapp.domain.event;

public class NoEvent extends SimulationEvent {

	@Override
	public String getMessage() {
		return "";
	}

}

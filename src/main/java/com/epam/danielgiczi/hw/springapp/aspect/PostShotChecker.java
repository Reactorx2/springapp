package com.epam.danielgiczi.hw.springapp.aspect;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.epam.danielgiczi.hw.springapp.domain.Shooter;
import com.epam.danielgiczi.hw.springapp.domain.Shot;

@Aspect
@Component("shotCheck")
@Order(value=90)
public class PostShotChecker {
	
	static final Logger logger = LoggerFactory.getLogger(PostShotChecker.class);
	
	@AfterReturning(
			pointcut="HuntsManPointcuts.shooterNextShot()",
			returning="retVal")
	public void validateHuntsManShot(JoinPoint joinPoint, Object retVal){
		Shooter shooter = (Shooter)joinPoint.getTarget();
		Shot shot = (Shot)retVal;
		double maxDamage = shooter.getMaximumDamage();
		if(maxDamage < shot.getDamage()){
			logger.warn("Hacked shot detected! It should not happen!");
		}
	}

}

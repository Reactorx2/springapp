package com.epam.danielgiczi.hw.springapp.domain;

public class Bunny implements Target {
	public enum Color {BROWN, WHITE, GREY, BLACK};
	
	private final String name;
	private final int ageInDays;
	private final Color color;
	private final Bunny father;
	private final Bunny mother;
	
	public Bunny(String name, int ageInDays, Color color, Bunny father, Bunny mother) {
		super();
		this.name = name;
		this.ageInDays = ageInDays;
		this.color = color;
		this.father = father;
		this.mother = mother;
	}

	@Override
	public String toString() {
		return "Bunny [name=" + name + ", ageInDays=" + ageInDays + ", color=" + color + ", father=" + father
				+ ", mother=" + mother + "]";
	}

	public String getName() {
		return name;
	}

	public int getAgeInDays() {
		return ageInDays;
	}

	public Color getColor() {
		return color;
	}

	public Bunny getFather() {
		return father;
	}

	public Bunny getMother() {
		return mother;
	}

	public int getStartingHealth() {
		return 20 + ageInDays / 10;
	}
	
}

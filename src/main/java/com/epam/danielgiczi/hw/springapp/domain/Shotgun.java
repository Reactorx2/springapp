package com.epam.danielgiczi.hw.springapp.domain;

public class Shotgun extends Gun {
	
	public enum AmmoType{BIRD, BUCK, SLUG};
	
	private final AmmoType ammoType;
	
	public Shotgun(String name, int damage, double chanceToHit, ShotFactory shotFactory, AmmoType ammoType) {
		super(name, damage, chanceToHit, shotFactory);
		this.ammoType = ammoType;
	}

	public AmmoType getAmmoType() {
		return ammoType;
	}

	@Override
	public String toString() {
		return "Shotgun [ammoType=" + ammoType + ", name=" + name + "]";
	}
}

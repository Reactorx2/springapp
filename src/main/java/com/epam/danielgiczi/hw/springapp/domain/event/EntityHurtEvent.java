package com.epam.danielgiczi.hw.springapp.domain.event;

import java.text.MessageFormat;

import com.epam.danielgiczi.hw.springapp.domain.ForestEntity;

public class EntityHurtEvent extends SimulationEvent {
	
	final ForestEntity who;
	final int remainingHealth;
	
	public EntityHurtEvent(ForestEntity who, int remainingHealth) {
		super();
		this.who = who;
		this.remainingHealth = remainingHealth;
	}
	
	public ForestEntity getEntity() {
		return who;
	}
	
	public int getRemainingHealth() {
		return remainingHealth;
	}

	@Override
	public String getMessage() {
		return MessageFormat.format("{0} is hurt, remaining health: {1}", who.getName(), remainingHealth);
	}
		
}

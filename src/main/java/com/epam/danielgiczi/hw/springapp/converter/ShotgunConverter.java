package com.epam.danielgiczi.hw.springapp.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import com.epam.danielgiczi.hw.springapp.domain.ShotFactory;
import com.epam.danielgiczi.hw.springapp.domain.Shotgun;
import com.epam.danielgiczi.hw.springapp.domain.Shotgun.AmmoType;

public class ShotgunConverter implements Converter<String, Shotgun> {

	@Autowired
	ShotFactory shotFactory;
	
	public Shotgun convert(String source) {
		String[] parts = source.split(";");

		int partCounter = 0;
		final String name = parts[partCounter++];
		final int damage = Integer.parseInt(parts[partCounter++].trim());
		final double chanceToHit = Double.parseDouble(parts[partCounter++]);
		final AmmoType ammoType = AmmoType.valueOf(parts[partCounter++]);

        return new Shotgun(name, damage, chanceToHit, shotFactory, ammoType);
	}
	
}

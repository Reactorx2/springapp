package com.epam.danielgiczi.hw.springapp.simulator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.danielgiczi.hw.springapp.domain.ForestEntity;
import com.epam.danielgiczi.hw.springapp.domain.Forest;
import com.epam.danielgiczi.hw.springapp.domain.Shooter;
import com.epam.danielgiczi.hw.springapp.domain.Shot;
import com.epam.danielgiczi.hw.springapp.domain.Target;
import com.epam.danielgiczi.hw.springapp.domain.TargetContainer;
import com.epam.danielgiczi.hw.springapp.domain.TargetContainerFactory;
import com.epam.danielgiczi.hw.springapp.domain.event.EventFacade;
import com.epam.danielgiczi.hw.springapp.domain.event.SimulationEvent;

public class HuntingSimulator extends Observable {
	
	final TargetContainerFactory targetContainerFactory;
	final EventFacade eventFacade;
	final protected Random random = new Random();
	final protected Forest forest;
	final protected ArrayList<TargetContainer> targetContainers = new ArrayList<TargetContainer>();
	final protected Logger logger = LoggerFactory.getLogger(HuntingSimulator.class);
	
	protected void step(){
		TargetContainer shooter = getRandomEntityOf(Shooter.class);
		TargetContainer target = getRandomEntityOf(Target.class);
		try{
    		Shot shot = ((Shooter)shooter.asTarget()).nextShot();
    		if(shot.isHit()){
    			handleHit(shooter, target, shot);
    		}
		} catch (IllegalStateException e){
		    logger.info("No gun assigned to " + shooter.asTarget().getName());
		}
	}

	private void handleHit(TargetContainer shooter, TargetContainer target, Shot shot) {
		target.hit(shot.getDamage());
		broadcastEvent(eventFacade.createShotEvent((Shooter)shooter.asTarget(), target.asTarget(), shot));
		broadcastEvent(eventFacade.createEntityHurtEvent(target, shot));
		if(shooter == target){
			broadcastEvent(eventFacade.createHuntingAccidentEvent(target));
		}
	}

	private void broadcastEvent(SimulationEvent event) {
		if(event != null){
			setChanged();
			notifyObservers(event);
		}
	}

	protected <T extends ForestEntity> TargetContainer getRandomEntityOf(Class<T> clazz) {
		TargetContainer resultContainer = null;
		
		LinkedList<TargetContainer> elements = new LinkedList<TargetContainer>();
		for(TargetContainer element : targetContainers){
			if(clazz.isAssignableFrom(element.asTarget().getClass()) && element.isEntityAlive()){
				elements.add(element);
			}
		}
		
		if(elements.size() != 0){
			resultContainer = elements.get(random.nextInt(elements.size()));
		}
		
		return resultContainer;
	}

	public HuntingSimulator(Forest forest, TargetContainerFactory targetContainerFactory, EventFacade eventFacade) {
		this.forest = forest;
		this.targetContainerFactory = targetContainerFactory;
		this.eventFacade = eventFacade;
		initTargetContainers();
	}

	private void initTargetContainers() {
		Collection<Target> targets = forest.getTargets();
		for(Target target : targets){
			targetContainers.add(targetContainerFactory.createContainerFor(target));
		}
	}
	
	public void simulate(){
		logger.info("Simulation started.");
		while(hasShooterAlive()){
			step();
		}
		broadcastEvent(eventFacade.createShootersAreDownEvent(getTargetsAlive()));
		logger.info("Simulation ended.");
	}

	private List<Target> getTargetsAlive() {
		LinkedList<Target> targetsAlive = new LinkedList<Target>();
		for(TargetContainer targetContainer : targetContainers){
			if(targetContainer.isEntityAlive()){
				targetsAlive.add(targetContainer.asTarget());
			}
		}
		return targetsAlive;
	}

	private boolean hasTargetAlive() {
		for(TargetContainer targetContainer : targetContainers){
			if(targetContainer.isEntityAlive()){
				return true;
			}
		}
		return false;
	}
	
	private boolean hasShooterAlive(){
		for(TargetContainer targetContainer : targetContainers){
			if(Shooter.class.isAssignableFrom(targetContainer.asTarget().getClass()) && targetContainer.isEntityAlive()){
				return true;
			}
		}
		return false;
	}
	
}

package com.epam.danielgiczi.hw.springapp.domain.event;

public abstract class SimulationEvent {
	public abstract String getMessage();
}

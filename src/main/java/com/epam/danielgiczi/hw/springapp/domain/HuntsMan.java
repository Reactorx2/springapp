package com.epam.danielgiczi.hw.springapp.domain;

public class HuntsMan extends AbstractShooter {
	private final String name;
	private final String address;
	private final int startingHealth;
	
	public HuntsMan(String name, String address, Gun gun, int health) {
		super(gun);
		this.name = name;
		this.address = address;
		this.startingHealth = health;
	}

	@Override
	public String toString() {
		return "HuntsMan [name=" + name + ", address=" + address + ", gun=" + gun + "]";
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public int getStartingHealth() {
		return startingHealth;
	}

}

package com.epam.danielgiczi.hw.springapp.domain.event;

import java.text.MessageFormat;
import java.util.List;

import com.epam.danielgiczi.hw.springapp.domain.Target;

public class ShootersAreDownEvent extends SimulationEvent {

	final List<Target> targetsAlive;
		
	public ShootersAreDownEvent(List<Target> targetsAlive) {
		super();
		this.targetsAlive = targetsAlive;
	}

	@Override
	public String getMessage() {
		return MessageFormat.format("All shooters are down. {0} targets alive: {1}", targetsAlive.size(), targetsAlive);
	}

}

package com.epam.danielgiczi.hw.springapp.domain;

public class Carrot {
	private final int kcal;

	public int getKcal() {
		return kcal;
	}

	public Carrot(int kcal) {
		super();
		this.kcal = kcal;
	}
	
}

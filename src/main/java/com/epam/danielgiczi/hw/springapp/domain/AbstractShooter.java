package com.epam.danielgiczi.hw.springapp.domain;

public abstract class AbstractShooter implements Shooter {

    protected final Gun gun;
    
    public Gun getGun() {
        return gun;
    }

    @Override
    public Shot nextShot() {
        return gun.nextShot();
    }
    
    @Override
    public final double getMaximumDamage(){
        return gun.getMaximumDamage();
    }

    public AbstractShooter(Gun gun) {
        super();
        this.gun = gun;
    }
    
    public abstract int getStartingHealth();

    public abstract String getName();

}

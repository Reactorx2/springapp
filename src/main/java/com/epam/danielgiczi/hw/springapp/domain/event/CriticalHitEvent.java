package com.epam.danielgiczi.hw.springapp.domain.event;

import java.text.MessageFormat;

import com.epam.danielgiczi.hw.springapp.domain.ForestEntity;

public class CriticalHitEvent extends HitEvent {

	public CriticalHitEvent(ForestEntity who, ForestEntity whom, int damage) {
		super(who, whom, damage);
	}
	
	@Override
	public String getMessage() {
		return MessageFormat.format("{0} dealt {1} critical damage to {2}", who.getName(), damage, whom.getName());
	}
	
}

package com.epam.danielgiczi.hw.springapp.domain.event;

import com.epam.danielgiczi.hw.springapp.domain.ForestEntity;

public abstract class EventBetweenTwoEntity extends SimulationEvent {
	final ForestEntity who;
	final ForestEntity whom;
	
	public EventBetweenTwoEntity(ForestEntity who, ForestEntity whom) {
		super();
		this.who = who;
		this.whom = whom;
	}
	
}

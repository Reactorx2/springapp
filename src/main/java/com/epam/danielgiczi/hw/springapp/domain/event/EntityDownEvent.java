package com.epam.danielgiczi.hw.springapp.domain.event;

import java.text.MessageFormat;

import com.epam.danielgiczi.hw.springapp.domain.ForestEntity;

public class EntityDownEvent extends EntityHurtEvent {
	
	public EntityDownEvent(ForestEntity who, int damage) {
		super(who, damage);
	}

	@Override
	public String getMessage() {
		return MessageFormat.format("{0} is down!", who.getName());
	}

}

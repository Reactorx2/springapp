package com.epam.danielgiczi.hw.springapp.domain.event;

import com.epam.danielgiczi.hw.springapp.domain.ForestEntity;

public abstract class ShotEvent extends EventBetweenTwoEntity {
	final int damage;
		
	public ShotEvent(ForestEntity who, ForestEntity whom, int damage) {
		super(who, whom);
		this.damage = damage;
	}
	
}

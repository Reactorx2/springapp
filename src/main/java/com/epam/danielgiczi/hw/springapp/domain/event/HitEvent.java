package com.epam.danielgiczi.hw.springapp.domain.event;

import java.text.MessageFormat;

import com.epam.danielgiczi.hw.springapp.domain.ForestEntity;

public class HitEvent extends ShotEvent {

	public HitEvent(ForestEntity who, ForestEntity whom, int damage) {
		super(who, whom, damage);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getMessage() {
		return MessageFormat.format("{0} dealt {1} damage to {2}", who.getName(), damage, whom.getName());
	}
	
}
